Sept 26, 2022:
	2x grab new AlN on Sapphire chip (1um / 650um)
	make pictures to ensure top side of AlN!
	Clean: 
	5min USL9 PRS3000, 
	acetone rinse, 
	3min USL9 acetone, 
	IPA rinse, 
	3min IPA USL9, 
	IPA beaker swirl, 
	N2 blow dry
	Microscope check
	Tepla: 600W, 600sccm O2 with cage, 2min
	10min dehydration HMDS machine
	HMDS vapour depo
	Spin
	ARN4450.10, 2min wait
	6000rpm, 4min wait
	bake 8min, 90C
	Microscope

	Chip 2 looks better than 1. No pictures. Due to long fridge hold of ARN there were more bubbles. Will still proceed with both!
	
	Now at Heidelberg. 2 chips 5 time HBAR design per chip. Defoc -4 and dose 250-254

	Hbar chip 1. 6min in MF321. 3 pieces (middle) are useful. Text gone

	Chip 2 4min MF321 also 3 middle pieces useful, but still text

	Dektak hbar chip1 --> see photos
	Dektak hbar chip2 --> see photos

	Took microgeaphs as well

Oct 3, 2022:
	At ICP Cl etcher. Starting a 30min O2 chamber clean at 50C
	Carrier wafer conditioning for 1min. After cleaning wafer with IPA
	Glued 2 chips on wafer and run recipe: "AlN etch J.Franse 1micron v4"
	In above recipe I added a 1min descum step at the end. 
	Stopped etch at step 8 around 2min in. Finished rest recipe and took out wafer/chips. Going to clean some resist of one chip (partly) and do some checks with the Dektak
	
	Dektak: first with then without resist: 2133nm - 1122nm

	So we have structures of 1122nm. Which is 1um AlN and about 100nm into sapphire. 
	We etched about 23mintues. We need to etch another 100nm of the top after cleaning the samples. 
	So...  we etch about 49nm/min. Let's etch another 2m20s after cleaning
	Cleaning chips in acetone and ipa. Each 2min USL9

	While waiting on plassys to pump down, I finish cleaning HBAR chips from this morning. 3min USL9 PRS, acetone and also IPA before N2 blow dry
	Afterwards spinning dicer resist: S1805, 1000rpm, 90C 5min

	Dektak before spin. Chip1: 
	Dektak before spin. Chip2: 

	Took pictures. Still some dirt. Not sure if from etching or from dirty resist. Need to check tomorrow after dicer and cleaning

Oct 10, 2022:
	diced the chips into shape

Oct 11, 2022:
	Roughly cleaned HBAR chips after dicing yesterday. 
	Diced 1 chip! Other (one with text) still has dicer PR on
	Clean: 3min USL9 acetone, IPA dip, N2 blow dry.
	Ready for Die bonder practise and "real" device. 
	Also have some spare sapphire chips for bonding sessions

Nov 22, 2022:
	flipped the chips (chip 1 Transmon, chip1 HBAR)

Nov 25, 2022:
	Took assembled chip from above into CR
	clean chip:
		carefully use swap with IPA on topside of HBAR
		N2 blow 
		O2 ashing in tepla
		F1 100 sccm O2, 100W 20s 
	PCB clean:
		2min USL9 acetone
		2min USL9 IPA
		*N2 blow dry
	Put on GE varnish and glued chip to it

Nov 30, 2022:
	wire bonding with Jason (Qutech)






